const moogose = require('mongoose');

const TweeSchema = new moogose.Schema({
  author: String,
  content: String,
  likes: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
});

module.exports = moogose.model('Tweet', TweeSchema);